open Hw1
open Printf

let () = begin
    for i = 0 to 20 do
        printf "%4d" (fib i);
        if i mod 5 = 0 then printf "\n"
    done;
    printf "\n\n";
    for i = 0 to 10 do
        for j = 0 to i do
            printf "%4d " (pascal (i, j))
        done;
        printf "\n"
    done;
    printf "\n\n";
    for i = 0 to 100 do
        if prime i then printf "%d " i
    done;
    printf "\n\n";
    printf "sigma (fun x -> x) 1 10 = %d\n" (sigma (fun x -> x) 1 10);
    printf "sigma (fun x -> x*x) 1 7 = %d\n" (sigma (fun x -> x*x) 1 7);
end