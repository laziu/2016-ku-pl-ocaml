module Problem1 = struct
  type aexp = 
  | Const of int
  | Var of string
  | Power of string * int
  | Times of aexp list
  | Sum of aexp list

  let rec diff : aexp * string -> aexp
  = fun (exp, var) -> match exp with
    | Const (x) -> Const(0)
    | Var (x) -> if x = var then Const(1) else Const(0)
    | Power (x, y) -> 
    begin
      if x = var then 
        if y = 0 then Const 0
        else if y = 1 then diff (Var(x), var)
        else if y = 2 then Times[Const(2); Var(x)]
        else Times[Const(y); Power(x, y-1)]
      else Const(0)
    end
    | Times _ -> Const 0
    | Sum l -> 
    begin
    let rec s el li =
      match el with
      | [] -> Sum(li)
      | hd::tl -> s tl (li @ [diff(hd, var)]) 
      in (s l [])
      end
end