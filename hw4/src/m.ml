exception UndefSemantics

type program = exp
and exp = 
  | CONST of int
  | VAR of var
  | ADD of exp * exp
  | SUB of exp * exp
  | MUL of exp * exp
  | DIV of exp * exp
  | ISZERO of exp
  | READ
  | IF of exp * exp * exp
  | LET of var * exp * exp
  | LETREC of var * var * exp * exp
  | PROC of var * exp
  | CALL of exp * exp
  | CALLREF of exp * var
  | SET of var * exp
  | SEQ of exp * exp
  | BEGIN of exp
and var = string

type value = 
    Int of int 
  | Bool of bool 
  | Closure of var * exp * env 
  | RecClosure of var * var * exp * env
and env = var -> loc
and loc = int
and mem = loc -> value

(*********************************)
(* implementation of environment *)
(*********************************)
(* empty env *)
let empty_env = fun x -> raise (Failure "Environment is empty")
(* extend the environment e with the binding (x,v), where x is a varaible and v is a value *)
let extend_env (x,v) e = fun y -> if x = y then v else (e y)
(* look up the environment e for the variable x *)
let apply_env e x = e x

(*********************************)
(* implementation of memory      *)
(*********************************)
let empty_mem = fun _ -> raise (Failure "Memory is empty")
let extend_mem (l,v) m = fun y -> if l = y then v else (m y)
let apply_mem m l = m l

(* NOTE: you don't need to understand how env and mem work *)

let counter = ref 0

(* calling 'new_location' produces a fresh memory location *)
let new_location () = counter:=!counter+1;!counter

let value2str v = 
  match v with
  | Int n -> string_of_int n
  | Bool b -> string_of_bool b
  | Closure (x,e,env) -> "Closure "
  | RecClosure (f,x,e,env) -> "RecClosure "^f

(*
let rec exp2str e = 
  match e with
  | CONST n      -> Printf.sprintf "CONST %d" n
  | VAR x        -> Printf.sprintf "VAR %s" x
  | ADD (e1, e2) -> Printf.sprintf "ADD (%s, %s)" (exp2str e1) (exp2str e2)
  | SUB (e1, e2) -> Printf.sprintf "SUB (%s, %s)" (exp2str e1) (exp2str e2)
  | MUL (e1, e2) -> Printf.sprintf "MUL (%s, %s)" (exp2str e1) (exp2str e2)
  | DIV (e1, e2) -> Printf.sprintf "DIV (%s, %s)" (exp2str e1) (exp2str e2)
  | ISZERO e     -> Printf.sprintf "ISZERO (%s)" (exp2str e)
  | READ -> "READ"
  | IF (e1, e2, e3) -> Printf.sprintf "IF (%s, %s, %s)" (exp2str e1) (exp2str e2) (exp2str e3)
  | LET (x, e2, e3) -> Printf.sprintf "LET (%s, %s, %s)" x (exp2str e2) (exp2str e3)
  | LETREC (f, x, e2, e3) -> Printf.sprintf "LETREC (%s, %s, %s, %s)" f x (exp2str e2) (exp2str e3)
  | PROC (x, e) -> Printf.sprintf "PROC (%s, %s)" x (exp2str e)
  | CALL (e1, e2) -> Printf.sprintf "CALL (%s, %s)" (exp2str e1) (exp2str e2)
  | CALLREF (e, y) -> Printf.sprintf "CALLREF (%s, %s)" (exp2str e) y
  | SET (x, e) -> Printf.sprintf "SET (%s, %s)" x (exp2str e)
  | SEQ (e1, e2) -> Printf.sprintf "SEQ (%s, %s)" (exp2str e1) (exp2str e2)
  | BEGIN e -> Printf.sprintf "BEGIN (%s)" (exp2str e) 
*)

(* TODO: Implement this function *)
let rec eval : exp -> env -> mem -> value * mem
=fun exp env mem -> 
  let arithmetical e1 e2 op =
    let val1, mem1 = eval e1 env mem in
    let val2, mem2 = eval e2 env mem1 in
    match val1, val2 with 
    | Int n1, Int n2 -> (Int (op n1 n2), mem2)
    | _ -> raise UndefSemantics in
  (*print_string ((exp2str exp)^"\n");*)
  match exp with
  | CONST n -> (Int n, mem)
  | VAR x -> (mem (env x), mem)
  | ADD (ex1, ex2) -> arithmetical ex1 ex2 ( + )
  | SUB (ex1, ex2) -> arithmetical ex1 ex2 ( - )
  | MUL (ex1, ex2) -> arithmetical ex1 ex2 ( * )
  | DIV (ex1, ex2) -> arithmetical ex1 ex2 ( / )
  | ISZERO ex -> 
      (match eval ex env mem with 
      | Int n, mem1 -> (Bool (n = 0), mem1) 
      | _ -> raise UndefSemantics)
  | READ -> (Int (read_int()), mem)
  | IF (ex1, ex2, ex3) ->
      (match eval ex1 env mem with
      | Bool b, mem1 -> eval (if b then ex2 else ex3) env mem1
      | _ -> raise UndefSemantics)
  | LET (x, ex1, ex2) ->
      let val1, mem1 = eval ex1 env mem in
      let l = new_location() in
      eval ex2 (extend_env (x, l) env) (extend_mem (l, val1) mem1)
  | LETREC (f, x, ex1, ex2) ->
      let l = new_location() in
      let val1 = RecClosure (f, x, ex1, extend_env (f, l) env) in
      eval ex2 (extend_env (f, l) env) (extend_mem (l, val1) mem)
  | PROC (x, ex) -> (Closure (x, ex, env), mem)
  | CALL (ex1, ex2) ->
      (match eval ex1 env mem with 
      (Closure (x, ex, env'), mem1) | (RecClosure (_, x, ex, env'), mem1) ->
          let v, mem2 = eval ex2 env mem1 in
          let l = new_location() in
          eval ex (extend_env (x, l) env') (extend_mem (l, v) mem2)
      | _ -> raise UndefSemantics)
  | CALLREF (ex1, y) ->
      (match eval ex1 env mem with
      (Closure (x, ex, env'), mem1) | (RecClosure (_, x, ex, env'), mem1) ->
          eval ex (extend_env (x, env y) env') mem1
      | _ -> raise UndefSemantics)
  | SET (x, ex) -> let v, mem1 = eval ex env mem in (v, extend_mem (env x, v) mem1)
  | SEQ (ex1, ex2) -> let v1, mem1 = eval ex1 env mem in eval ex2 env mem1
  | BEGIN ex -> eval ex env mem

let run : program -> value
=fun pgm -> 
  let (v,m) = eval pgm empty_env empty_mem in
    v
