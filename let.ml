type program = exp
and exp = 
|   CONST of int
|   VAR of string
|   ADD of exp * exp
|   SUB of exp * exp
|   ISZERO of exp
|   IF of exp * exp * exp
|   LET of string * exp * exp
|   READ

(* 1 + 2 *)
let pgm1 = ADD (CONST 1, CONST 2)

(* if iszero 1 then 2 else 3 *)
let pgm2 = IF (ISZERO (CONST 1), CONST 2, CONST 3)

(* let x = read in x - 1 *)
let pgm3 = LET( "x", READ, SUB (VAR "x", CONST 1) )

type value = Int of int | Bool of bool
type env = (string * value) list

let empty_env = []
let extend_env : string -> value -> env -> env =
    fun x v env -> (x, v)::env

let rec lookup_env : string -> env -> value =
    fun x env ->
        match env with 
        |   [] -> raise (Failure "env is empty")
        |   (y, v)::tl -> if x = y then v else lookup_env x tl

let eval_bop : (int -> int -> int) -> exp -> exp -> env -> value =
    fun bop e1 e2 env ->
        let v1 = eval env e1 in
        let v2 = eval env e2 in
        begin
            match v1, v2 with
            |   Int n1, Int n2 -> Int (n1 + n2)
            |   _, _ -> raise (Failure "Addition requires two ints")
        end

let rec eval : env -> exp -> value =
    fun env exp ->
        match exp with 
        |   CONST n -> Int n
        |   VAR x -> lookup_env x env
        |   ADD (e1, e2) -> 
                let v1 = eval env e1 in
                let v2 = eval env e2 in
                begin
                    match v1, v2 with
                    |   Int n1, Int n2 -> Int (n1 + n2)
                    |   _, _ -> raise (Failure "Addition requires two ints")
                end
        |   SUB (e1, e2) ->
                let v1 = eval env e1 in
                let v2 = eval env e2 in
                begin
                    match v1, v2 with
                    |   Int n1, Int n2 -> Int (n1 - n2)
                    |   _ -> raise (Failure "Substraction requires two ints")
                end
        |   ISZERO e ->
                let v = eval env e in
                begin
                    match v with
                    |   Int n -> if n = 0 then Bool true else Bool false
                    |   Bool b -> raise (Failure "iszero requires int")
                end
        |   IF (e1, e2, e3) ->
                let v1 = evan env e1 in
                begin
                    match v1 with
                    |   Bool b -> if b = true then eval env e2 else eval env e3
                    |   _ -> raise (Failure "if requires bool for the first arg")
                end
        |   LET (x, e, body) ->
                let v = eval env e in
                let env' = extend_env x v env in
                eval env' body
        |   READ -> Int (read_int ())

let run : program -> value =
    fun 